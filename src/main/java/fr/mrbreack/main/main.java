package fr.mrbreack.main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class main extends JavaPlugin{

    public static main instance;

    public Map < Player, Integer > TeleportSecond = new HashMap <>();
    public Map < Player, Integer > PlayerOUT = new HashMap <>();

    @Override
    public void onEnable(){

        instance = this;

        System.out.println("Plugin Parchemin On");

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new PlayerInteractListener(), instance);

        instance.getCommand("parchemingive").setExecutor(new CmdParchemin());

        new TimeUpdate().runTaskTimer(instance, 20L, 20L);
    }

    @Override
    public void onDisable(){
        System.out.println("Plugin Parchemin OFF");
    }

    public static main getInstance() { return instance; }
}
