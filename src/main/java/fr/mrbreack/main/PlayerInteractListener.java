package fr.mrbreack.main;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerInteractListener implements Listener{

    public main instance = main.getInstance();

    @EventHandler
    public void playerInteract(PlayerInteractEvent e)
    {

        ItemStack it = e.getItem();

        final Player player = e.getPlayer();
        final Action action = e.getAction();


        if (e.useItemInHand() == null)
            return;
        if (e.getItem() == null)
            return;
        if (e.getMaterial() == null)
            return;
        if (e.getHandlers() == null)
            return;

        if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            if (it.getItemMeta().getDisplayName().equalsIgnoreCase(("&6&l• Parchemin de téléportation").replace("&", "§"))) {

                Gui(player);
            }
        }
    }


    @EventHandler
    public void onClickInventory(InventoryClickEvent e){
        ItemStack it = e.getCurrentItem();
        Player p = ( Player ) e.getWhoClicked();
        if (e.getInventory() == null)
            return;
        if (e.getCurrentItem() == null)
            return;
        if (e.getCurrentItem().getType().equals(Material.AIR))
            return;
        if (!e.getCurrentItem().hasItemMeta())
            return;

        if (e.getCurrentItem().getItemMeta().getDisplayName() == null) {
            return;
        }

        if (e.getCurrentItem().hasItemMeta()) {

            if (it.getType().equals(Material.DARK_OAK_DOOR) && it.getItemMeta().getDisplayName().equalsIgnoreCase(( "&6&lMaison" ).replace("&" , "§"))) {
                e.setCancelled(true);

                p.closeInventory();

                if(!instance.PlayerOUT.containsKey(p)) {

                    instance.TeleportSecond.put(p , 5);

                    p.sendMessage("§6Vous lisez le sortilège du parchemin vous avez besoin d'une concentration absolue !");
                    new BukkitRunnable(){

                        @Override
                        public void run(){

                            if (instance.PlayerOUT.containsKey(p)) {
                                return;
                            }


                            if(p.getBedSpawnLocation() == null){
                                p.sendMessage("§cLa téléportation a échoué vous n'avez pas de foyer !");
                                return;
                            }
                            p.getWorld().playSound(p.getLocation() , Sound.ENTITY_ENDERMAN_TELEPORT , 4 , 1);

                            for (ItemStack item : p.getInventory().getContents()) {

                                if(item != null) {
                                    if (item.getItemMeta().getDisplayName().equalsIgnoreCase(( "&6&l• Parchemin de téléportation" ).replace("&" , "§"))) {
                                        int amount = item.getAmount() - 1;

                                        item.setAmount(amount);
                                        break;
                                    }
                                }
                            }

                            p.teleport(p.getBedSpawnLocation());
                        }

                    }.runTaskLater(instance , 100);
                }else {
                    p.sendMessage("§cVous venez de vous intérrompre reprenez votre souffle ! (" + instance.PlayerOUT.get(p) + " secondes)");
                }
            }

            if (it.getType().equals(Material.WARPED_DOOR) && it.getItemMeta().getDisplayName().equalsIgnoreCase(( "&6&lSpawn" ).replace("&" , "§"))) {
                e.setCancelled(true);

                p.closeInventory();
                if(!instance.PlayerOUT.containsKey(p)) {
                    instance.TeleportSecond.put(p , 5);
                    p.sendMessage("§6Vous lisez le sortilège du parchemin vous avez besoin d'une concentration absolue !");

                    new BukkitRunnable(){

                        @Override
                        public void run(){

                            if (instance.PlayerOUT.containsKey(p)) {
                                return;
                            }

                            p.getWorld().playSound(p.getLocation() , Sound.ENTITY_ENDERMAN_TELEPORT , 4 , 1);

                            for (ItemStack item : p.getInventory().getContents()) {

                                if(item != null) {
                                    if (item.getItemMeta().getDisplayName().equalsIgnoreCase(( "&6&l• Parchemin de téléportation" ).replace("&" , "§"))) {
                                        int amount = item.getAmount() - 1;

                                        item.setAmount(amount);
                                        break;
                                    }
                                }
                            }

                            Location location = new Location(Bukkit.getWorld("world"),
                                    189.5,
                                    174,
                                    -256.5,
                                    136,
                                    2);
                            
                            p.teleport(location);

                        }

                    }.runTaskLater(instance , 120);
                }else {
                    p.sendMessage("§cVous venez de vous intérrompre reprenez votre souffle ! (" + instance.PlayerOUT.get(p) + " secondes)");
                }
            }

            if (it.getType().equals(Material.BARRIER) && it.getItemMeta().getDisplayName().equalsIgnoreCase(( "&c&lAnnuler !" ).replace("&" , "§"))) {

                p.closeInventory();

                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void playerMove(PlayerMoveEvent e){

        if(instance.TeleportSecond.containsKey(e.getPlayer())) {
            instance.TeleportSecond.remove(e.getPlayer());
            instance.PlayerOUT.put(e.getPlayer(), 10);

            e.getPlayer().sendMessage("§cVous venez de vous intérrompre !");
        }

    }

    public void Gui(Player p){

        Inventory inv = Bukkit.createInventory(null, 9*1, ("&e&lParchemin de Téléportation").replace("&", "§"));

        ItemStack itemStack = new ItemStack(Material.DARK_OAK_DOOR, 1);
        ItemMeta itemStackM = itemStack.getItemMeta();
        itemStackM.setDisplayName(("&6&lMaison").replace("&", "§"));
        itemStack.setItemMeta(itemStackM);

        ItemStack itemStack1 = new ItemStack(Material.WARPED_DOOR, 1);
        ItemMeta itemStackM1 = itemStack1.getItemMeta();
        itemStackM1.setDisplayName(("&6&lSpawn").replace("&", "§"));
        itemStack1.setItemMeta(itemStackM1);

        ItemStack itemStack2 = new ItemStack(Material.BARRIER, 1);
        ItemMeta itemStackM2 = itemStack2.getItemMeta();
        itemStackM2.setDisplayName(("&c&lAnnuler !").replace("&", "§"));
        itemStack2.setItemMeta(itemStackM2);

        inv.setItem(3, itemStack);
        inv.setItem(5, itemStack1);
        inv.setItem(8, itemStack2);

        p.openInventory(inv);

    }
}
