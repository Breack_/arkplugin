package fr.mrbreack.main;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import static java.lang.Math.cos;

public class TimeUpdate extends BukkitRunnable {

    private main instance = main.getInstance();

    @Override
    public void run(){

        if(instance.TeleportSecond.keySet() != null) {
            for ( Player p : instance.TeleportSecond.keySet() ) {

                if (p == null) {
                    return;
                }

                if (instance.TeleportSecond.get(p).equals(0)) {
                    p.getWorld().playSound(p.getLocation() , Sound.ITEM_BOOK_PAGE_TURN , 4 , 1);

                    p.spawnParticle(Particle.PORTAL , p.getLocation() , 50);

                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR , TextComponent.fromLegacyText("§6" + instance.TeleportSecond.get(p) + " seconde(s) restant"));

                    instance.TeleportSecond.remove(p);
                } else {

                    p.getWorld().playSound(p.getLocation() , Sound.ITEM_BOOK_PAGE_TURN , 4 , 1);

                    p.spawnParticle(Particle.PORTAL , p.getLocation() , 50);

                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR , TextComponent.fromLegacyText("§6" + instance.TeleportSecond.get(p) + " seconde(s) restant"));

                    instance.TeleportSecond.put(p , instance.TeleportSecond.get(p) - 1);
                }
            }
        }else{

        }

        if(instance.TeleportSecond.keySet() != null) {
            for ( Player p : instance.PlayerOUT.keySet() ) {

                if (p == null) {
                    return;
                }

                if (instance.PlayerOUT.get(p).equals(0)) {
                    instance.PlayerOUT.remove(p);
                } else {
                    instance.PlayerOUT.put(p , instance.PlayerOUT.get(p) - 1);
                }
            }
        }else{

        }
    }
}
